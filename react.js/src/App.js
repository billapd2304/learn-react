import React from 'react';

function Welcome (props){
  return <h1>Halo,{props.name}</h1>
}

function App () {
  return (
    <div>
      <Welcome name="Salsa"/>
      <Welcome name="billa"/>
      <Welcome name="cantik"/>
    </div>
  );
}
export default App;